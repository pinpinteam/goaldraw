using UnityEngine;
using UnityEditor;
using System.IO;

namespace Pinpin.Editor
{

	[CustomEditor(typeof(GameDatas))]
	public class GameDatasEditor: UnityEditor.Editor
	{
	
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();
			
			if (GUILayout.Button("Reset player prefs datas"))
				 PlayerPrefs.DeleteAll();

			if (GUILayout.Button("Reset saved datas"))
			{
				if (File.Exists(GameDatas.SavedDatas.path))
					File.Delete(GameDatas.SavedDatas.path);
			}

		}
		
	}

}