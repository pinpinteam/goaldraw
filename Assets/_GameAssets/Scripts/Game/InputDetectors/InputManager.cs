using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pinpin.InputControllers
{

	public class InputManager : MonoBehaviour
	{

		private static InputManager singleton { get; set; }

		[SerializeField] private SlideArea m_slideArea;
		[SerializeField] private float m_sensitivity = 1f;
		public static float sensitivity
		{
			get { return (singleton.m_sensitivity); }
			set { singleton.m_sensitivity = value; }
		}

		public static Vector2 moveDelta
		{
			get { return (InputManager.singleton.m_slideArea.delta * InputManager.singleton.m_sensitivity); }
		}

		public static bool isMoving
		{
			get { return (InputManager.singleton.m_slideArea.isMoving); }
		}

		private void Awake ()
		{
			if (InputManager.singleton != null)
			{
				GameObject.Destroy(this.gameObject);
				return;
			}
			InputManager.singleton = this;
		}

		/*private void Start ()
		{
			sensitivity = GameManager.datas.sensitivity;
		}*/

	}

}