﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goal : MonoBehaviour
{
	[SerializeField] private List<ParticleSystem> m_particlesList;

	public static event Action OnGoal;

	private void OnTriggerEnter(Collider other)
	{
		for (int i = 0; i < m_particlesList.Count; i++)
		{
			m_particlesList[i].Play();
		}

		if (other.GetComponent<Ball>() && OnGoal != null)
			OnGoal();
	}
}
