﻿using MoreMountains.NiceVibrations;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pinpin
{
	public class PlayerController : MonoBehaviour
	{
		[SerializeField] private int m_maxPoint = 25;
		[SerializeField] private float m_distanceBetweenPoint = 0.5f;
		[SerializeField] private int m_minPointNumber;
		[SerializeField] private float m_ballHeight;
		[SerializeField] private float m_startRange;
		[SerializeField] private float m_maxRange;
		[SerializeField] private FingerTrail m_trailPrefab;

		private List<Vector3> positionList = new List<Vector3>();
		private Vector3 lastPosition;
		private Vector3 heightOffset;

		private Ball m_ball;
		private RagdollPlayer m_ragdoll;
		private Action DoAction;
		private FingerTrail m_trail;

		private void Awake()
		{
			SetModeVoid();
		}

		public void Init(Ball ball, RagdollPlayer ragdoll)
		{
			heightOffset = Vector3.up * m_ballHeight;
			m_ball = ball;
			m_ragdoll = ragdoll;
			SetModeWaitForClick();
		}

		public void Reset()
		{
			m_ball = null;

			if(m_trail != null)
				Destroy(m_trail.gameObject);

			positionList.Clear();
		}

		private void ResetTrail()
		{
			Destroy(m_trail.gameObject);
			m_trail = Instantiate(m_trailPrefab);
		}

		// Update is called once per frame
		void Update()
		{
			DoAction();
		}

		private void SetModeWaitForClick()
		{
			DoAction = DoActionWaitForClick;
		}

		private void DoActionWaitForClick()
		{
#if UNITY_EDITOR
			if (Input.GetMouseButtonDown(0))
			{
				StartDraw();
			}
#else
			if(Input.GetTouch(0).phase == TouchPhase.Began)
			{
				StartDraw();
			}
#endif
		}

		private void SetModeDraw()
		{
			DoAction = DoActionDraw;
		}

		private void DoActionDraw()
		{

#if UNITY_EDITOR
				if (Input.GetMouseButton(0))
				{
					Draw();

				}
				if (Input.GetMouseButtonUp(0))
				{
					EndDraw();
				}

#else
				if (Input.GetTouch(0).phase == TouchPhase.Moved)
				{
					Draw();
				}

				if (Input.GetTouch(0).phase == TouchPhase.Ended)
				{
					EndDraw();
				}
#endif


		}


		private bool IsInFirstTouchRange(Vector3 point)
		{
			return Vector3.Distance(point, m_ball.transform.position) <= m_startRange;
		}
		
		private bool IsInTouchRange(Vector3 point)
		{
			return Vector3.Distance(point, m_ball.transform.position) <= m_maxRange;
		}

		private bool IsFarFromLastPoint(Vector3 point)
		{
			return Vector3.Distance(point, lastPosition) > m_distanceBetweenPoint;
		}

		private bool IsPointListFull()
		{
			return positionList.Count <= m_maxPoint;
		}

		private void StartDraw()
		{
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			if (Physics.Raycast(ray, out RaycastHit hitInfo))
			{
				if (!IsInFirstTouchRange(hitInfo.point))
					return;

				MMVibrationManager.Haptic(HapticTypes.MediumImpact);
				lastPosition = hitInfo.point + heightOffset;

				m_trail = Instantiate(m_trailPrefab);
				m_trail.transform.position = lastPosition;
				m_trail.GetComponent<TrailRenderer>().Clear();

				SetModeDraw();
			}
		}

		private void Draw()
		{
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			if (Physics.Raycast(ray, out RaycastHit hitInfo))
			{
				if (IsFarFromLastPoint(hitInfo.point + heightOffset) && IsPointListFull() && IsInTouchRange(hitInfo.point))
				{
					MMVibrationManager.Haptic(HapticTypes.LightImpact);
					lastPosition = hitInfo.point + heightOffset;
					m_trail.transform.position = lastPosition;
					positionList.Add(lastPosition);
				}
			}
		}

		private void EndDraw()
		{
			if (positionList.Count >= m_minPointNumber)
			{
				StartShoot();
				MMVibrationManager.Haptic(HapticTypes.MediumImpact);
			}
			else
			{
				positionList.Clear();
				ResetTrail();
				SetModeWaitForClick();
				MMVibrationManager.Haptic(HapticTypes.MediumImpact);
			}
		}

		private void StartShoot()
		{
			m_ragdoll.StartAnimation();
			m_ragdoll.OnShootAnimationEnded += RagdollPlayer_OnShootAnimationEnded;
			SetModeVoid();
		}

		private void RagdollPlayer_OnShootAnimationEnded()
		{
			m_ball.SetModeTrack(positionList);
			m_ragdoll.OnShootAnimationEnded -= RagdollPlayer_OnShootAnimationEnded;
		}

		private void SetModeVoid()
		{
			DoAction = DoActionVoid;
		}

		private void DoActionVoid()
		{

		}
	}
}
