﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathZone : MonoBehaviour
{
	public static event Action OnBallTouched;

	private void OnTriggerEnter(Collider other)
	{
		if (other.GetComponent<Ball>() && OnBallTouched != null)
			OnBallTouched.Invoke();
	}
}
