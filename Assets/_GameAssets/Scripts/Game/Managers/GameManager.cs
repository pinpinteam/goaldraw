﻿using System.Collections.Generic;
using UnityEngine;
using System.Numerics;
using Pinpin.Helpers;
using Pinpin.Scene.MainScene.UI;
using System;
using PaperPlaneTools;
using URandom = UnityEngine.Random;
using UVector2 = UnityEngine.Vector2;
using UVector3 = UnityEngine.Vector3;
using TMPro;
using Pinpin.UI;
using Pipin;
using MoreMountains.NiceVibrations;
#if MADPINPIN
using MadboxAnalytics = MadBox.Services.ExternalServices.Analytics;
#endif

namespace Pinpin
{
	public class GameManager : Singleton<GameManager>
	{
		public static Action OnLevelUp;
		public static Action<BigInteger> OnLevelEnd;
		[SerializeField] private MainPanel m_mainPanel;
		[SerializeField] GameObject m_money;
		[SerializeField] private Transform m_cameraTransform;
		// Timers
		private float m_vibrationTimer = 0f;
		
		[SerializeField] SpriteRenderer m_backGroundRenderer;
		
		private Coroutine m_limitedBoosterCoroutine;
		private bool m_mustShowLimitedBooster = false;
		private BigInteger m_currentLevelGoldEarned = 0;
		private string m_lastPatternName;

		public event Action OnGameWin;
		public event Action OnGameLoose;
		public event Action OnGameStart;

		private new void Awake ()
		{
			ApplicationManager.onRewardedVideoAvailabilityChange += OnRewardedVideoAvailabilityChange;
			Input.multiTouchEnabled = false;
			base.Awake();
		}

		// Use this for initialization
		void Start()
		{
			ComputeNeededXP();
		}
		
		private void OnDestroy()
		{

		}

		private void ComputeNeededXP()
		{
            if (ApplicationManager.datas.level > 0)
                ApplicationManager.xpMax = MathHelper.Fibonnaci(ApplicationManager.datas.level) * ApplicationManager.config.game.baseXpToLevelUp + MathHelper.Fibonnaci(ApplicationManager.datas.level - 1) * ApplicationManager.config.game.baseXpToLevelUp;
            else
                ApplicationManager.xpMax = MathHelper.Fibonnaci(ApplicationManager.datas.level) * ApplicationManager.config.game.baseXpToLevelUp;
        }

		private void OnRewardedVideoAvailabilityChange ( bool value )
		{
			if (value && m_mustShowLimitedBooster)
			{
				m_mustShowLimitedBooster = false;
				m_mainPanel.DisplayLimitedBooster();
			}
		}
		
		

		private void Update()
		{
			if (Input.GetKeyDown(KeyCode.T))
				ApplicationManager.datas.haveCompleteTutorial = false;
			if (Input.GetKeyDown(KeyCode.U))
				LevelUp();
				ApplicationManager.datas.UpdateBoostersTime(Time.deltaTime);
			if (m_vibrationTimer > 0)
				m_vibrationTimer -= Time.deltaTime;
			
		}
		

		public void AddCurrency(BigInteger currencyToAdd)
		{
			m_currentLevelGoldEarned += currencyToAdd;
			ApplicationManager.AddCoins(currencyToAdd);
		}
		public void AddMillion()
		{
#if CUSTOM
			AddCurrency(1000000);
#endif
		}
		

		private void LevelUp()
		{
			ApplicationManager.datas.xp -= ApplicationManager.xpMax;
			//AnalyticsManager.NewDesignEvent("level_achieved", "level_number", ApplicationManager.datas.level, true);
			ComputeNeededXP();
			RateBox.Instance.IncrementCustomCounter();
			if (OnLevelUp != null)
				OnLevelUp.Invoke();
		}

		public void DisplayLimitedBoosterManager ( float time )
		{
			if (m_limitedBoosterCoroutine != null)
			{
				StopCoroutine(m_limitedBoosterCoroutine);
			}
			m_limitedBoosterCoroutine = StartCoroutine(DisplayLimitedBoosterCoroutine(time));
		}

		private System.Collections.IEnumerator DisplayLimitedBoosterCoroutine ( float time )
		{
			yield return new WaitForSeconds(time);
#if MADPINPIN
			if (ApplicationManager.canWatchRewardedVideo)
			{
				m_mainPanel.DisplayLimitedBooster();
			}
			else
				m_mustShowLimitedBooster = true;
#endif
		}
		
        public void CashIn(BigInteger _value, UVector3 _position)
		{
			GameObject money = Instantiate(m_money);
			money.transform.position = new UVector3(_position.x, _position.y, 9);
			money.GetComponentInChildren<TextMeshPro>().text = "+ " + MathHelper.ConvertToEngineeringNotation(_value , 4);
			Destroy(money, 1.1f);
			AddCurrency(_value );
		}

		public void StartGame()
		{
#if MADPINPIN
			MadboxAnalytics.GameEvents.StartGame("normal", ApplicationManager.datas.level));
#endif

			// TODO start game
			LevelManager.Instance.InitLevel();
			OnGameStart();
			EnableEvent();
		}

		public void ResetGame()
		{
#if MADPINPIN
			MadboxAnalytics.GameEvents.StartGame("normal", ApplicationManager.datas.level));
#endif

			// TODO reset game
			LevelManager.Instance.ResetLevel();
			EnableEvent();
		}

		private void Obstacle_OnBallTouch()
		{
			LevelFailed();
		}

		private void Goal_OnGoal()
		{
			LevelComplete();
		}
		
		private void Ball_OnFailed()
		{
			LevelFailed();
		}
		private void DeathZone_OnBallTouched()
		{
			LevelFailed();
		}

		public void StartShoot()
		{

		}

		public void LevelComplete ()
		{
#if MADPINPIN
			MadboxAnalytics.GameEvents.EndGame(MadboxAnalytics.COMPLETION_STATUS.COMPLETE, (float)m_currentLevelGoldEarned, m_lastPatternName, 100);
#endif
			OnLevelEnd.Invoke(m_currentLevelGoldEarned);
			m_currentLevelGoldEarned = 0;

			DisableEvent();

			MMVibrationManager.Haptic(HapticTypes.Success);

			if (OnGameWin != null)
				OnGameWin();
		}

		public void LevelFailed ()
		{
#if MADPINPIN
			MadboxAnalytics.GameEvents.EndGame(MadboxAnalytics.COMPLETION_STATUS.FAIL, (float)m_currentLevelGoldEarned, m_lastPatternName);
#endif
			m_currentLevelGoldEarned = 0;

			DisableEvent();

			MMVibrationManager.Haptic(HapticTypes.Failure);

			if (OnGameLoose != null)
				OnGameLoose();
		}

		private void EnableEvent()
		{
			Goal.OnGoal += Goal_OnGoal;
			DeathZone.OnBallTouched += DeathZone_OnBallTouched;
			Ball.OnFailed += Ball_OnFailed;
			Obstacle.OnBallTouch += Obstacle_OnBallTouch;
		}

		private void DisableEvent()
		{
			Goal.OnGoal -= Goal_OnGoal;
			DeathZone.OnBallTouched -= DeathZone_OnBallTouched;
			Ball.OnFailed -= Ball_OnFailed;
			Obstacle.OnBallTouch -= Obstacle_OnBallTouch;
		}

	}
}
