﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pinpin
{
	public class LevelManager : Singleton<LevelManager>
	{
		[SerializeField] private PlayerController playerController;

		[SerializeField] private Transform m_ballPosition;
		[SerializeField] private Transform m_characterPosition;

		[SerializeField] private Ball m_ballPrefab;
		[SerializeField] private RagdollPlayer m_ragdollPrefab;

		[SerializeField] private List<Level> m_levelList;

		private Level m_actualLevel;
		private Ball m_ball;
		private RagdollPlayer m_ragdoll;
		private int m_levelIndex = 0;

		// Start is called before the first frame update
		void Start()
		{
			
		}

		public void InitLevel()
		{
			CleanLevel();
			m_ball = Instantiate(m_ballPrefab, m_ballPosition.position, m_ballPosition.rotation);
			m_ball.Init();
			m_ragdoll = Instantiate(m_ragdollPrefab, m_characterPosition.position, m_characterPosition.rotation);
			playerController.Init(m_ball, m_ragdoll);

			m_levelIndex = Random.Range(0, m_levelList.Count - 1);
			if (ApplicationManager.datas.level <= 4)
				m_levelIndex = ApplicationManager.datas.level;
			m_actualLevel = Instantiate(m_levelList[m_levelIndex]);


		}

		public void ResetLevel()
		{
			CleanLevel();
			m_ball = Instantiate(m_ballPrefab, m_ballPosition.position, m_ballPosition.rotation);
			m_ball.Init();
			m_ragdoll = Instantiate(m_ragdollPrefab, m_characterPosition.position, m_characterPosition.rotation);
			playerController.Init(m_ball, m_ragdoll);

			m_actualLevel = Instantiate(m_levelList[m_levelIndex]);
		}

		public void CleanLevel()
		{
			Destroy(m_ball);
			Destroy(m_ragdoll);
			playerController.Reset();
			Destroy(m_actualLevel);
		}

		// Update is called once per frame
		void Update()
		{
        
		}
	}
}
