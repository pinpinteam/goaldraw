﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagdollPlayer : MonoBehaviour
{
	private Animator m_animator;

	public AnimationEvent OnEvent;
	public event Action OnShootAnimationEnded;

	private void Awake()
	{
		m_animator = GetComponentInChildren<Animator>();
	}

	// Update is called once per frame
	void Update()
    {
        
    }

	public void StartAnimation()
	{
		m_animator.Play("Dribble");
	}

	private void OnRunEnded()
	{
		m_animator.Play("SoccerPass");
	}

	public void OnShootEnded()
	{
		OnShootAnimationEnded();
	}

	private void OnDestroy()
	{
		Destroy(gameObject);
	}


}
