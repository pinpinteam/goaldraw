﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
	[SerializeField] private float m_speed = 5;
	[SerializeField, Range(0,1)] private float m_speedDrag = 5;
	[SerializeField] private float m_projectionSpeed = 5;
	[SerializeField] private float m_minSpeed = 0.5f;
	[SerializeField] private float m_rotationSpeed;
	[SerializeField] private GameObject m_childObject;

	public static List<Ball> list = new List<Ball>();
	public static event Action OnFailed;

	private List<Vector3> m_positionList = new List<Vector3>();
	private Vector3 lastPosition;

	private Rigidbody m_rigidbody;
	private int m_positionIndex = 0;

	private Action DoAction;

	// Update is called once per frame
	void Update()
    {
		DoAction();
	}

	public void Init()
	{
		list.Add(this);
		m_rigidbody = GetComponent<Rigidbody>();
		SetModeNormal();
	}

	public void Bounce(Vector3 collisionPoint)
	{
		Vector3 direction = collisionPoint - transform.position;

		m_rigidbody.velocity = direction * 10;
	}

	private void SetModeNormal()
	{
		m_rigidbody.isKinematic = true;
		DoAction = DoActionNormal;
		m_positionIndex = 0;
	}

	private void DoActionNormal()
	{

	}

	public void SetModeTrack(List<Vector3> positionList)
	{
		m_positionList = positionList;
		DoAction = DoActionTrack;
	}

	private void DoActionTrack()
	{
		if (transform.position == m_positionList[m_positionIndex])
		{
			if (m_positionIndex == m_positionList.Count - 1)
			{
				SetModeFree();
				return;
			}

			else
			{
				m_speed *= m_speedDrag;
				m_positionIndex++;
				transform.LookAt(m_positionList[m_positionIndex]);
			}
		}

		m_childObject.transform.rotation = Quaternion.AngleAxis(m_rotationSpeed, m_childObject.transform.right) * m_childObject.transform.rotation;
		transform.position = Vector3.MoveTowards(transform.position, m_positionList[m_positionIndex], m_speed);
	}

	public void SetModeFree()
	{
		m_rigidbody.isKinematic = false;
		m_rigidbody.velocity = (transform.position - m_positionList[m_positionIndex - 1]).normalized * m_projectionSpeed;
		m_rigidbody.angularVelocity = new Vector3(20,0,0);
		DoAction = DoActionFree;
	}

	private void DoActionFree()
	{
		if (m_rigidbody.velocity.magnitude <= m_minSpeed || transform.position.y <= -10)
		{
			SetModeNormal();
			if(OnFailed != null)
				OnFailed();
		}
	}

	private void OnDestroy()
	{
		Destroy(this.gameObject);
		list.Remove(this);
	}
}
