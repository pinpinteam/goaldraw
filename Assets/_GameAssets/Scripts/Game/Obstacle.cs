﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pipin
{
	public class Obstacle : MonoBehaviour
	{
		public enum State { Static, Loop, Path }

		[SerializeField] private State m_state;

		[SerializeField] private float m_speed;

		[Header("Path Settings")]
		[SerializeField] private List<Transform> m_pathList;

		[Header("Loop Settings")]
		[SerializeField] private Transform m_spawnLoop;
		[SerializeField] private Transform m_endLoop;

		public static event Action OnBallTouch;

		private Action DoAction;

		private Animator m_animator;
		private Rigidbody[] m_rigRigidbodys;
		private Collider[] m_rigColliders;
		private Collider m_collider;
		private Rigidbody m_rigidbody;

		private bool isOnRagdoll = false;

		private int pathIndex = 0;

		private void Awake()
		{
			SetModeVoid();
			m_animator = GetComponent<Animator>();
			Init();
			m_rigRigidbodys = GetComponentsInChildren<Rigidbody>();
			m_rigColliders = GetComponentsInChildren<Collider>();
			m_collider = GetComponent<Collider>();
			m_rigidbody = GetComponent<Rigidbody>();
			DisableRagdoll();

			m_collider.enabled = true;
			m_rigidbody.isKinematic = false;

		}

		private void Init()
		{
			switch (m_state)
			{
				case State.Path:
					SetModePath();
					break;
				case State.Loop:
					SetModeLoop();
					break;
				case State.Static:
					SetModeStatic();
					break;
			}
		}

		private void SetModeStatic()
		{
			m_animator.SetInteger("state", (int)m_state);
			DoAction = DoActionVoid;
		}

		// Update is called once per frame
		void Update()
		{
			DoAction();
		}

		private void SetModeVoid()
		{
			DoAction = DoActionVoid;
		}

		private void DoActionVoid()
		{

		}

		private void SetModeLoop()
		{
			m_animator.SetInteger("state", (int)m_state);
			DoAction = DoActionLoop;
			transform.LookAt(m_endLoop.position);
		}

		private void DoActionLoop()
		{
			if (transform.position == m_endLoop.position)
			{
				transform.position = m_spawnLoop.position;
			}

			transform.position = Vector3.MoveTowards(transform.position, m_endLoop.position, m_speed);
		}

		private void SetModePath()
		{
			m_animator.SetInteger("state", (int)m_state);
			DoAction = DoActionPath;
		}

		private void DoActionPath()
		{
			if (Vector3.Distance(transform.position, m_pathList[pathIndex].position) <= 0.2)
			{
				pathIndex = pathIndex == m_pathList.Count - 1 ? pathIndex = 0 : pathIndex + 1;
				transform.LookAt(m_pathList[pathIndex].position);
			}

			transform.position = Vector3.MoveTowards(transform.position, m_pathList[pathIndex].position, m_speed);
		}

		private void DisableRagdoll()
		{
			foreach (Collider collider in m_rigColliders)
			{
				collider.enabled = false;
			}

			foreach (Rigidbody rigidbody in m_rigRigidbodys)
			{
				rigidbody.isKinematic = true;
			}
		}

		private void EnableRagdoll()
		{
			isOnRagdoll = true;
			foreach (Collider collider in m_rigColliders)
			{
				collider.enabled = true;
			}

			foreach (Rigidbody rigidbody in m_rigRigidbodys)
			{
				rigidbody.isKinematic = false;
			}
		}

		private void OnCollisionEnter(Collision collision)
		{
			Ball ball = collision.gameObject.GetComponent<Ball>();

			if(ball != null && OnBallTouch != null)
			{
				m_animator.enabled = false;
				EnableRagdoll();
				ball.SetModeFree();
				ball.Bounce(transform.position);
				OnBallTouch();
			}
			if(collision.gameObject.GetComponentInParent<Obstacle>() && !isOnRagdoll)
			{
				m_animator.enabled = false;
				EnableRagdoll();
			}
		}
	}
}
