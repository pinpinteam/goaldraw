﻿using UnityEngine;
using System;
using System.Numerics;
using Pinpin.Helpers;

namespace Pinpin
{

	[CreateAssetMenu(fileName = "GameConfig", menuName = "Game/GameConfig", order = 1)]
	public class GameConfig: ScriptableObject
	{

		[Serializable]
		public class ApplicationConfig
		{
			public string version = "1.0";
			public int targetFrameRate = 60;
			public int splashScreenDuration;
			public bool enableRemoteSettings = false;
			public bool enablePurchasing = false;
			public bool enableAds = false;
			public bool enableOfflineEarning;
			public string amplitudeAPIKey;
		}

		[Serializable]
		public sealed class AdsSettings
		{
			public enum Banner
			{
				None,
				Bottom,
				Top
			}

			public int				lifetimeCollectBeforeInterstitial = 1;
			[Min(0f)] public float	delayBetweenInterstitials = 0f;
			[Min(0f)] public float	delayFirstInterstitial = 0f;
			public Banner			banner;
			public bool				useSmartBanner = false;

			public string interstitialTestAdUnit;
			public string rewardedVideoTestAdUnit;

			public bool isFirstInterstitial { get; internal set; }
		}

		[Serializable]
		public sealed class GameSettings
		{
			public int baseXpToLevelUp = 1000;
			
			[Header("Upgrades")]
			public int[] upgradeBaseCost;
			public float[] boosterTimer;
			public float limitedBoosterTimer;
			[Min(0f)] public float delayFirstLimitedBooster = 0f;
			[Min(0f)] public float delayBetweenLimitedBooster = 0f;
		}

		public ApplicationConfig	application;
		public AdsSettings			ads;
		public GameSettings			game;

		public void Inititialize ()
		{
			if (this.application.enableRemoteSettings)
				RemoteSettings.Updated += new RemoteSettings.UpdatedEventHandler(this.OnRemoteSettingsUpdate);
		}

		private void OnDisable ()
		{
			RemoteSettings.Updated -= new RemoteSettings.UpdatedEventHandler(this.OnRemoteSettingsUpdate);
		}

		private void OnRemoteSettingsUpdate ()
		{
			Debug.Log("GameConfig - OnRemoteSettingsUpdate()");
		}

		public BigInteger GetUpgradeCost (Upgrade.UpgradeType upgradeType, int level)
		{
			return MathHelper.Fibonnaci(level) * game.upgradeBaseCost[Mathf.Min((int)upgradeType, game.upgradeBaseCost.Length - 1)];
		}

	}

}