using UnityEngine;
using UnityEngine.Audio;
using System;

namespace Pinpin
{

	[CreateAssetMenu(fileName = "GameAssets", menuName = "Game/GameAssets", order = 1)]
	public class GameAssets: ScriptableObject
	{
		public AudioMixer	audioMixer;

	}
}