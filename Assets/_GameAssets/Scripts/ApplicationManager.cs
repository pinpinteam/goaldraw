using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.Collections;
using System.Globalization;
using System.Linq;
using Pinpin.UI;
using Pinpin.Scene;
using UnityScene = UnityEngine.SceneManagement.Scene;
using GameScene = Pinpin.Scene.ASceneManager.Scene;
using Pinpin.Types;
using Banner = Pinpin.GameConfig.AdsSettings.Banner;
using PaperPlaneTools;
using System.Numerics;
#if MADPINPIN
using MadboxAds = MadBox.Services.ExternalServices.Ads;
#endif

namespace Pinpin
{

	[DisallowMultipleComponent]
	public class ApplicationManager : MonoBehaviour
	{
		[SerializeField] private GameConfig m_gameConfig;

		[SerializeField] private GameAssets m_gameAssets;
		[SerializeField] private GameDatas m_gameDatas;
		[SerializeField] private RateBoxPrefabScript m_rateBoxPrefab;
		[SerializeField] private bool m_isRewardedAvailable;
#if MADPINPIN
		[SerializeField] private MadboxAds.GDPRPopup m_GDPRPopup;
#endif
		[SerializeField] private Transform m_GDPRParent;
		private static ApplicationManager singleton { get; set; }
		public static bool isInitialized { get { return (ApplicationManager.singleton != null); } }
		public static ASceneManager currentSceneManager { get; private set; }
		public static bool networkReachable { get { return (Application.internetReachability != NetworkReachability.NotReachable); } }
		public static string applicationVersion { get { return (ApplicationManager.config.application.version); } }
		public static bool canTakeOfflineEarning = true;
		public static bool oeNeedToBeClaimed;

		public static TimeSpan timeSinceLastAppQuit { get; private set; }

		public static Action<bool> onNetworkReachabilityChange;
		public static Action<bool> onRewardedVideoAvailabilityChange;

		public static BigInteger xpMax { set; get; }

		public static GameDatas datas
		{
			get { return (ApplicationManager.singleton.m_gameDatas); }
		}

		public static GameConfig config
		{
			get { return (ApplicationManager.singleton.m_gameConfig); }
		}

		public static GameAssets assets
		{
			get { return (ApplicationManager.singleton.m_gameAssets); }
		}

		public static string currentScene
		{
			get { return (SceneManager.GetActiveScene().name); }
		}

		public static bool playerLostVIP { get; private set; }

		private void Awake()
		{
			if (ApplicationManager.singleton != null)
			{
				GameObject.Destroy(this.gameObject);
#if DEBUG
				Debug.Log("ApplicationManager - Removing duplicate Object");
#endif
				return;
			}

#if DEBUG
			Debug.Log("ApplicationManager - Awake()");
#endif

			ApplicationManager.singleton = this;
			GameObject.DontDestroyOnLoad(this.gameObject);

			// GameEvents
			SceneManager.sceneLoaded += OnSceneLoaded;
			ASceneManager.onSceneReady += OnSceneReady;

			// Loading Screen
			LoadingScreen.ShowSplashScreen();
			LoadingScreen.HideSplashScreen();

			// Culture & Localization
			ApplicationManager.CheckCulture();

			// Init config
			if (ApplicationManager.config.application.enableRemoteSettings && Application.isEditor)
				ApplicationManager.singleton.m_gameConfig = ScriptableObject.Instantiate(ApplicationManager.config);
			ApplicationManager.config.Inititialize();


		}

		private void Start ()
		{
			// Ads
#if MADPINPIN_ADS
			if (ApplicationManager.isAdsEnabled)
			{
				MadboxAds.AdsCenter.SetCustomGDPR(m_GDPRPopup, m_GDPRParent);
				MadboxAds.AdsCenter.Init(config.ads.delayBetweenInterstitials, config.ads.delayBetweenInterstitials, 0, false, "9cd6f835", "9cd731bd").Then(OnAdInitialized).Catch(( e ) => {
					Debug.LogError(e.Message);
				});
			}
			else
			{
				LoadDatas();
			}
#else
			LoadDatas();
#endif
		}

#if MADPINPIN_ADS
		private void OnAdInitialized()
		{
			StartCoroutine(ChecRewardedVideoAvailability());
			LoadDatas();
		}
#endif

		private void LoadDatas()
		{
			GameDatas.onLoadComplete += OnGameDatasLoaded;
			m_gameDatas.LoadDatas();
		}

		private void OnGameDatasLoaded ()
		{
			StartCoroutine(Load());
		}

		private bool m_purchasingInitialised = false;
		private IEnumerator Load()
		{
			LoadingScreen.Show();
			

			// Purchasing
			if (ApplicationManager.isPurchasingEnabled)
			{
				PurchasingManager.Initialize(ApplicationManager.OnPurchasingManagerInitializationComplete, ApplicationManager.OnProductBought);
				while (m_purchasingInitialised == false && ApplicationManager.networkReachable)
					yield return null;
			}


			/*#if !UNITY_EDITOR
						m_rateBoxPrefab.minCustomEventsCount = RemoteSettings.GetInt("ratebox_shot_count", 11);
						m_rateBoxPrefab.postponeCooldownInHours = RemoteSettings.GetInt("ratebox_time_between_show", 8);
						m_rateBoxPrefab.delayAfterLaunchInHours = RemoteSettings.GetInt("ratebox_time_before_show", 2) / 60f;
#endif*/
			m_rateBoxPrefab.Init();


			ApplicationManager.datas.sessionCount++;

			// Load Settings
			ApplicationManager.datas.LoadSettings();
			Application.targetFrameRate = ApplicationManager.config.application.targetFrameRate;
			QualitySettings.vSyncCount = 1;
			Time.fixedDeltaTime = 1f / Application.targetFrameRate;

			// Timestamp checks
			if (ApplicationManager.networkReachable)
			{
#if DEBUG
				Debug.Log("ApplicationManager - Checking for timestamps");
#endif
				ApplicationManager.singleton.StartCoroutine(ApplicationManager.singleton.CheckAppLaunchTimes());
			}
			else
			{
#if DEBUG
				Debug.LogWarning("ApplicationManager - No network connection. Cannot retrieve timestamps");
#endif
			}
			((InitScene.InitSceneManager)currentSceneManager).ContinueLoading();
		}

		private void OnApplicationQuit()
		{
			if (!oeNeedToBeClaimed)
			{
				SetupOffineEarningValues();
			}
			ApplicationManager.datas.SaveDatas();
		}

		private void OnApplicationPause(bool pause)
		{
			Debug.Log("OnApplicationPause : " + pause);
			if (pause)
			{
				if (!oeNeedToBeClaimed)
				{
					SetupOffineEarningValues();
				}
				ApplicationManager.datas.SaveDatas();
			}
			else
			{
				if (ApplicationManager.networkReachable)
				{
					if (utcInitialized)
						ApplicationManager.singleton.StartCoroutine(ApplicationManager.singleton.CheckAppFocusTimes());
					else
						ApplicationManager.singleton.StartCoroutine(ApplicationManager.singleton.CheckAppLaunchTimes());
				}
			}
		}

		private void OnDestroy()
		{
			ApplicationManager.datas.SaveDatas();
		}

		private void Update()
		{
			if (m_timeBeforeSave > 0f)
			{
				m_timeBeforeSave -= Time.unscaledDeltaTime;
				if (m_timeBeforeSave <= 0f)
					ApplicationManager.datas.SaveDatas();
			}

		}

		private void SetupOffineEarningValues()
		{
			if (!ApplicationManager.utcInitialized)
				return;

			canTakeOfflineEarning = true;
			m_gameDatas.lastApplicationQuitTime = utcTime;
			ApplicationManager.datas.SaveDatas();
			int hours = RemoteSettings.GetInt("OfflineEarningNotificationTime", 48);
		}

		public static void SetCurrentSceneManager(ASceneManager current)
		{
			ApplicationManager.currentSceneManager = current;
		}

		public static void LoadScene(GameScene scene)
		{
			ApplicationManager.LoadScene(scene.ToString());
		}

		private static void LoadScene(string name)
		{
#if DEBUG
			Debug.Log("ApplicationManager - Loading scene '" + name + "'");
#endif

			ApplicationManager.SaveGameDatas();

			LoadingScreen.Show();
			SceneManager.LoadSceneAsync(name);
		}

		public static void MuteSound()
		{
#if DEBUG
			Debug.Log("ApplicationManager - Muting sound.");
#endif
			ApplicationManager.assets.audioMixer.SetFloat("Master", -80f);
		}

		public static void UnMuteSound()
		{
#if DEBUG
			Debug.Log("ApplicationManager - UnMuting sound.");
#endif
			ApplicationManager.assets.audioMixer.SetFloat("Master", singleton.m_gameDatas.isSoundActive ? 0f : -80f);
		}

		private void OnSceneLoaded(UnityScene scene, LoadSceneMode mode)
		{
			if (mode != LoadSceneMode.Additive)
			{
#if DEBUG
				Debug.Log("ApplicationManager - Scene changed to '" + scene.name + "'");
#endif
			}
		}

		private void OnSceneReady()
		{
#if DEBUG
			Debug.Log("ApplicationManager - Scene Ready.");
#endif
			LoadingScreen.Hide();
		}

		static float m_timeBeforeSave = 0f;
		public static void SaveGameDatas()
		{
			m_timeBeforeSave = 1f;
		}

		public static void Quit()
		{
#if DEBUG
			Debug.Log("ApplicationManager - Quitting...");
#endif

#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
#else
			Application.Quit();
#endif
		}

#region Ads
#if MADPINPIN_ADS
		public static bool isAdsEnabled { get { return (ApplicationManager.config.application.enableAds); } }

		public static bool canWatchInterstitial
		{
			get
			{
				if (!ApplicationManager.isAdsEnabled)
					return false;
				if (ApplicationManager.datas.lifetimeCollect <= ApplicationManager.config.ads.lifetimeCollectBeforeInterstitial)
					return false;
		
				if (MadboxAds.AdsCenter.Instance.IsInterstitialReady)
				{
					return true;
				}
				return false;
			}
		}

		public static bool canWatchRewardedVideo
		{
			get
			{
#if UNITY_EDITOR
				if (!isInitialized || !singleton.m_isRewardedAvailable)
					return false;
#endif
				if (!ApplicationManager.isAdsEnabled)
					return (false);
		
				return (MadboxAds.AdsCenter.Instance.IsRewardedReady);
			}
		}
#endif
#endregion

#region Culture & Localization

		public static CultureInfo currentCulture { get; private set; }

		// Gets the current culture
		private static void CheckCulture()
		{
			SystemLanguage language = Application.systemLanguage;
			CultureInfo[] cultures = CultureInfo.GetCultures(CultureTypes.SpecificCultures);
			ApplicationManager.currentCulture = cultures.FirstOrDefault(x => x.EnglishName.Contains(Enum.GetName(language.GetType(), language)));

			if (ApplicationManager.currentCulture == null)
				ApplicationManager.currentCulture = cultures.FirstOrDefault(x => x.EnglishName.Contains("English"));

#if DEBUG
			Debug.Log("ApplicationManager - Current culture set to [" + ApplicationManager.currentCulture + "]");
#endif
		}

#endregion
#if MADPINPIN_ADS
		bool lastNetworkReachable = false;
		private IEnumerator CheckNetworkReachability()
		{
			WaitForSeconds checkTime = new WaitForSeconds(1);
			while (true)
			{
				if (lastNetworkReachable != networkReachable)
				{
					lastNetworkReachable = networkReachable;
					if (onNetworkReachabilityChange != null)
						onNetworkReachabilityChange.Invoke(networkReachable);
				}
				yield return checkTime;
			}
		}

		bool lastRewardedVideoAvailable = false;
		private IEnumerator ChecRewardedVideoAvailability()
		{
			WaitForSeconds checkTime = new WaitForSeconds(1);
			while (true)
			{
				if (lastRewardedVideoAvailable != canWatchRewardedVideo)
				{
					lastRewardedVideoAvailable = canWatchRewardedVideo;
					if (onRewardedVideoAvailabilityChange != null)
						onRewardedVideoAvailabilityChange.Invoke(lastRewardedVideoAvailable);
				}
				yield return checkTime;
			}
			yield return null;
		}
#endif
#region App Timestamps

		public static bool utcInitialized { get; set; }
		public static uint lastUtcLaunchTime { get; private set; }
		public static uint utcLaunchTime { get; private set; }
		public static uint utcTime { get { return (ApplicationManager.utcLaunchTime + (uint)Time.realtimeSinceStartup); } }
		public static uint utcTomorrow { get; private set; }
		public static bool newDay { get; set; }
		public static TimeSpan timeSinceFirstLaunch { get; private set; }

		// Retrieve time and do checks
		private IEnumerator CheckAppLaunchTimes()
		{
			UnityEngine.Networking.UnityWebRequest request;
			request = UnityEngine.Networking.UnityWebRequest.Get("https://timestamp.pinpinteam.com/timestamp.php");

			yield return (request.SendWebRequest());

			if (request.responseCode != 200)
			{
				ApplicationManager.utcInitialized = false;
				yield break; // NO INTERNET
			}

			string html = request.downloadHandler.text;
			uint utcTimestamp = uint.Parse(html);
			DateTime epoch = new System.DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);

			DateTime lastLaunchDate = epoch.AddSeconds(m_gameDatas.lastApplicationLaunchTime);
			DateTime lastQuitDate = epoch.AddSeconds(m_gameDatas.lastApplicationQuitTime);
			DateTime launchDate = epoch.AddSeconds(utcTimestamp);

			// store last app launch
			if (m_gameDatas.firstApplicationLaunchTime == 0)
				m_gameDatas.firstApplicationLaunchTime = utcTimestamp;
			DateTime firstApplicationLaunchTime = epoch.AddSeconds(m_gameDatas.firstApplicationLaunchTime);
			ApplicationManager.timeSinceFirstLaunch = launchDate - firstApplicationLaunchTime;
			ApplicationManager.utcLaunchTime = utcTimestamp;
			//ApplicationManager.utcTomorrow = (uint)(new System.DateTime(launchDate.Year, launchDate.Month, launchDate.Day, 0, 0, 0, 0, DateTimeKind.Utc).AddDays(1.0) - epoch).TotalSeconds;

			// check new day
			if (lastQuitDate.DayOfYear != launchDate.DayOfYear)
				newDay = true;

			// check consecutive days
			int daysDiff = launchDate.DayOfYear - lastLaunchDate.DayOfYear;
			if (daysDiff == 1)
				m_gameDatas.consecutiveDaysLaunches++;
			else if (daysDiff > 1)
				m_gameDatas.consecutiveDaysLaunches = 1;

			//Store time since last app launch
			if (m_gameDatas.lastApplicationQuitTime != 0)
				timeSinceLastAppQuit = launchDate - lastQuitDate;


			utcInitialized = true;
			if (onUTCTimeUpdated != null)
				onUTCTimeUpdated.Invoke();

#if DEBUG
			Debug.Log("ApplicationManager - Last app Launch date : " + lastLaunchDate);
			Debug.Log("ApplicationManager - Current app Launch date : " + launchDate);
			Debug.Log("ApplicationManager - Consecutive days launches : " + m_gameDatas.consecutiveDaysLaunches);
			Debug.Log("ApplicationManager - Time since last app launch : " + timeSinceLastAppQuit.TotalMinutes + " minutes");
#endif

		}


		public static Action onUTCTimeUpdated;

		// Retrieve time and do checks
		private IEnumerator CheckAppFocusTimes()
		{
			UnityEngine.Networking.UnityWebRequest request;
			request = UnityEngine.Networking.UnityWebRequest.Get("https://timestamp.pinpinteam.com/timestamp.php");

			yield return (request.SendWebRequest());

			if (request.responseCode != 200)
			{
				yield break; // NO INTERNET
			}

			string html = request.downloadHandler.text;
			uint utcTimestamp = uint.Parse(html);
			DateTime epoch = new System.DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);

			DateTime lastQuitDate = epoch.AddSeconds(m_gameDatas.lastApplicationQuitTime);
			DateTime launchDate = epoch.AddSeconds(utcTimestamp);

			// store last app launch
			m_gameDatas.lastApplicationLaunchTime = utcTimestamp;
			//ApplicationManager.utcTomorrow = (uint)(new System.DateTime(launchDate.Year, launchDate.Month, launchDate.Day, 0, 0, 0, 0, DateTimeKind.Utc).AddDays(1.0) - epoch).TotalSeconds;

			// check new day
			if (lastQuitDate.DayOfYear != launchDate.DayOfYear)
				newDay = true;

			//Store time since last app launch
			timeSinceLastAppQuit = launchDate - lastQuitDate;

#if DEBUG
			Debug.Log("ApplicationManager - Current app Focus date : " + launchDate);
			Debug.Log("ApplicationManager - Time since last app quit : " + timeSinceLastAppQuit.TotalMinutes + " minutes");
#endif
			
			if (onUTCTimeUpdated != null)
				onUTCTimeUpdated.Invoke();
		}

#endregion

#region Purchasing

		public static bool isPurchasingEnabled { get { return (ApplicationManager.config.application.enablePurchasing); } }

		// Called when Purchasing availibility changed
		// call when your reactivate internet if previously fail cause of non network connection)
		private static void OnPurchasingManagerInitializationComplete(bool isInitialized)
		{
#if DEBUG
			Debug.Log("ApplicationManager - Purchasing Manager initialized [" + isInitialized + "]");
#endif
			singleton.m_purchasingInitialised = true;
		}

		// Called when product bought
		private static void OnProductBought(string productId)
		{
#if DEBUG
			Debug.Log("ApplicationManager - IAP Product bought [" + productId + "]");
#endif

			switch (productId)
			{
				default:
#if DEBUG
					Debug.LogError("ApplicationManager - Unrecognized product id");
#endif
					break;
			}
		}

#endregion


		public static System.Action onCoinsChange;
		public static void AddCoins(BigInteger value)
		{
			singleton.m_gameDatas.coins += value;
			SaveGameDatas();
			if (onCoinsChange != null)
				onCoinsChange.Invoke();
		}

		public static void RemoveCoins(BigInteger value)
		{
			singleton.m_gameDatas.coins -= value;
			SaveGameDatas();
			if (onCoinsChange != null)
				onCoinsChange.Invoke();
		}

		public static System.Action onDiamondsChange;
		public static void AddDiamonds ( BigInteger value )
		{
			singleton.m_gameDatas.diamonds += value;
			SaveGameDatas();
			if (onDiamondsChange != null)
				onDiamondsChange.Invoke();
		}

		public static void RemoveDiamonds ( BigInteger value )
		{
			singleton.m_gameDatas.diamonds -= value;
			SaveGameDatas();
			if (onDiamondsChange != null)
				onDiamondsChange.Invoke();
		}
	}
}