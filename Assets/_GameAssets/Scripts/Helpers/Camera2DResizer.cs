﻿using UnityEngine;

[ExecuteInEditMode]
public class Camera2DResizer: MonoBehaviour
{

	public enum ResizeMode
	{
		None,
		Crop,
		Zoom
	}

	[SerializeField] private float targetOrthographicSize = 5f;
	[SerializeField] private Vector2 targetAspectRatio = new Vector2(16, 9);
	[SerializeField] private ResizeMode largestResizeMode;
	[SerializeField] private ResizeMode narrowResizeMode;
	
	private new UnityEngine.Camera camera { get; set; }
	private Vector2 screenSize { get; set; }

	private void Awake ()
	{
		camera = GetComponent<UnityEngine.Camera>();
		this.OnScreenSizeChanged();
	}

	private void OnScreenSizeChanged ()
	{
		float currentRatio = Screen.width / (float)Screen.height;
		float targetRatio = targetAspectRatio.x / targetAspectRatio.y;
		
		this.DefaultSize();
		if (Mathf.Approximately(currentRatio, targetRatio))
			return;

		if (currentRatio > targetRatio)
			this.LargestResize(currentRatio, targetRatio);
		else
			this.NarrowResize(currentRatio, targetRatio);
	}

	private void NarrowResize ( float currentRatio, float targetRatio )
	{
		float delta = currentRatio / targetRatio;
		switch (this.narrowResizeMode)
		{

			case ResizeMode.Crop:
				float yOffset = (1f - delta) / 2f;
				this.camera.rect = new Rect(0, yOffset, 1, delta);
				break;

			case ResizeMode.Zoom:
				this.camera.orthographicSize = targetOrthographicSize / delta;
				break;
		}
	}

	private void LargestResize ( float currentRatio, float targetRatio )
	{
		float delta = targetRatio / currentRatio;
		switch (this.largestResizeMode)
		{

			case ResizeMode.Crop:
				float xOffset = (1f - delta) / 2f;
				this.camera.rect = new Rect(xOffset, 0, delta, 1);
				break;

			case ResizeMode.Zoom:
				this.camera.orthographicSize = targetOrthographicSize * delta;
				break;
		}
	}

	private void DefaultSize ()
	{
		this.camera.rect = new Rect(0, 0, 1, 1);
		this.camera.orthographicSize = targetOrthographicSize;
	}

	
	private void OnValidate ()
	{
		if (this.camera == null)
			this.camera = GetComponent<UnityEngine.Camera>();

		this.OnScreenSizeChanged();
		this.screenSize = new Vector2(Screen.width, Screen.height);
	}

	private void Update ()
	{
		if (screenSize.x != Screen.width || screenSize.y != Screen.height)
			this.OnScreenSizeChanged();
	}
}
