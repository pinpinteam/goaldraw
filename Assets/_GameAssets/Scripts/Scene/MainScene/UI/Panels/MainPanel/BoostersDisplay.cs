﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Pinpin
{

	// DEPRECATED /!\
	public class BoostersDisplay : MonoBehaviour
	{
		[SerializeField] private Image[] m_boosters;
		[SerializeField] private Boosters.BoosterType[] m_boostersType;

		private float[] m_boostersDurations;

		// Use this for initialization
		void Start()
		{
			m_boostersDurations = new float[ApplicationManager.config.game.boosterTimer.Length];
			System.Array.Copy(ApplicationManager.config.game.boosterTimer, m_boostersDurations, ApplicationManager.config.game.boosterTimer.Length);
			GameDatas.OnBoosterTimeIncreased += UpdateMaxBoosterTime;
			UpdateMaxBoostersTime();
		}

		// Update is called once per frame
		void Update()
		{
			for (int i = 0; i < m_boosters.Length; i++)
			{
				if (ApplicationManager.datas.IsBoosterActive(m_boostersType[i]))
				{
					m_boosters[i].fillAmount = ApplicationManager.datas.BoosterRemainingTime(m_boostersType[i]) / m_boostersDurations[(int)m_boostersType[i]];
				}
				else
				{
					m_boosters[i].gameObject.SetActive(false);
				}
			}
		}

		private void UpdateMaxBoostersTime()
		{
			foreach (Boosters.BoosterType type in m_boostersType)
			{
				UpdateMaxBoosterTime(type);
			}
		}

		private void UpdateMaxBoosterTime(Boosters.BoosterType type)
		{
			int arrayPosition = System.Array.IndexOf(m_boostersType, type);
			if (arrayPosition >= 0) {
				m_boosters[arrayPosition].gameObject.SetActive(true);
				m_boostersDurations[(int)type] = ApplicationManager.datas.BoosterRemainingTime(type);
			}
		}

		private void OnDestroy()
		{
			GameDatas.OnBoosterTimeIncreased -= UpdateMaxBoosterTime;
		}
	}
}
