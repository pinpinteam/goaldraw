﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Pinpin.UI
{

	public class TabButton : MonoBehaviour
	{
		public enum Tab
		{
			Upgrades,
			Boosters,
			Shop,
			Farms
		}

		public static Action<Tab> onClic;
		[SerializeField] private PushButton m_tabButton;
		[SerializeField] private Tab m_tabType;
		[SerializeField] private Image background;
		[SerializeField] Color m_tabColor;
		[SerializeField] Color m_tabSelectedColor;


		void Start ()
		{
			m_tabButton.onClick += OnUpgradeButtonClick;
		}

		private void OnUpgradeButtonClick ()
		{
			if (onClic != null)
				onClic.Invoke(m_tabType);
		}

		public void SetActive (bool active)
		{
			background.color = active ? m_tabSelectedColor : m_tabColor;
		}

	}

}