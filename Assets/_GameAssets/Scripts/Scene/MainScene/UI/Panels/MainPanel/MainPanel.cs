﻿using AdsManagement;
using Pinpin.UI;
using UnityEngine;
using UnityEngine.UI;
using Pinpin.Helpers;
using System.Numerics;
using DG.Tweening;

using UVector3 = UnityEngine.Vector3;
using System;
using PaperPlaneTools;
using System.Collections;
using URandom = UnityEngine.Random;
#if MADPINPIN
using MadboxAnalytics = MadBox.Services.ExternalServices.Analytics;
#endif
using System.Collections.Generic;

namespace Pinpin.Scene.MainScene.UI
{

	public sealed class MainPanel : AUIPanel
	{
		[SerializeField] private Canvas m_cheatCanvas;

		[Header("Top UI")]
		[SerializeField] private ParticleSystem m_moneyPS;
		[SerializeField] private Text moneyText;

		[SerializeField] GameObject m_uiToggle;
		[SerializeField] GameObject m_millionButton;


		[SerializeField] PushButton m_startButton;

		[Header("Tabs Section")]
		[SerializeField] private GameObject[] m_tabs;
		[SerializeField] private TabButton[] m_tabButtons;

		private Sequence m_bounceSequence;
		private int m_lastLimitedBoosterIndex;
		private Coroutine m_limitedBoosterCoroutine;
		
		public static Action<Upgrade.UpgradeType> OnUpgrade;
		public static Action<Boosters.BoosterType> OnDisplayLimitedBooster;

		private new MainSceneUIManager UIManager
		{
			get { return (base.UIManager as MainSceneUIManager); }
		}
		
		protected override void Awake ()
		{
			base.Awake();
		}

		void Start ()
		{
			ApplicationManager.onCoinsChange += UpdateCoinsDisplay;
			ApplicationManager.onUTCTimeUpdated += OnUTCTimeUpdated;
			BoosterDisplay.onLimitedBoosterDisplayExpire += OnLimitedBoosterDisplayExpire;
			TabButton.onClic += OnClickTab;
			GameManager.OnLevelEnd += ShowEndLevelPopup;
			UpdateCoinsDisplay();

			UpgradeButton.onClick += OnUpgradeButtonClick;
			BoosterButton.onClick += OnBoosterButtonClick;
			m_startButton.onClick += OnStartButtonClick;


			CheckOfflineEarning();
#if CAPTURE || UNITY_EDITOR
			m_cheatCanvas.gameObject.SetActive(true);
#else
			Destroy(m_cheatCanvas.gameObject);
#endif
			m_lastLimitedBoosterIndex = Enum.GetValues(typeof(Boosters.BoosterType)).Length;
			GameManager.Instance.DisplayLimitedBoosterManager(ApplicationManager.config.game.delayFirstLimitedBooster);
		}
		

		protected override void OnDestroy ()
		{
			ApplicationManager.onCoinsChange -= UpdateCoinsDisplay;
			ApplicationManager.onUTCTimeUpdated -= OnUTCTimeUpdated;
			UpgradeButton.onClick -= OnUpgradeButtonClick;
			BoosterButton.onClick -= OnBoosterButtonClick;
			BoosterDisplay.onLimitedBoosterDisplayExpire -= OnLimitedBoosterDisplayExpire;
			GameManager.OnLevelEnd -= ShowEndLevelPopup;
			base.OnDestroy();
		}

		private void OnUpgradeButtonClick(Upgrade.UpgradeType type)
		{
			BigInteger cost = ApplicationManager.config.GetUpgradeCost(type, ApplicationManager.datas.GetUpgradeLevel((int)type) + 1);

			if (cost <= ApplicationManager.datas.coins)
			{
				int level = ApplicationManager.datas.GetUpgradeLevel((int)type)+1;

#if MADPINPIN
				Dictionary<string, object> args = new Dictionary<string, object>();
				args.Add("type", type.ToString());
				args.Add("level", level);
				args.Add("cost", cost);
				MadboxAnalytics.AnalyticsCenter.SendEvent("Upgrade", args);
#endif

				ApplicationManager.datas.IncreaseUpgradeLevel((int)type);
				ApplicationManager.RemoveCoins(cost);

				if (OnUpgrade != null)
					OnUpgrade.Invoke(type);
			}
			
        }
		

		private Boosters.BoosterType m_clickedBoosterType;
		private bool m_rewardedVideoStarted;
		private bool m_needOfflineEarningCheck;

		private void OnBoosterButtonClick ( Boosters.BoosterType type, bool isLimited )
		{
			m_clickedBoosterType = type;
#if MADPINPIN_ADS
			if (!m_rewardedVideoStarted)
			{
				if (UIManager.sceneManager.ShowRewardedVideo(OnBoosterVideoEnd, "booster_" + type.ToString()))
					m_rewardedVideoStarted = true;
			}
#endif
		}
		

		private void ActivateBooster ()
		{
#if MADPINPIN
			Dictionary<string, object> args = new Dictionary<string, object>();
			args.Add("type", m_clickedBoosterType.ToString());
			MadboxAnalytics.AnalyticsCenter.SendEvent("LimitedBoosterActivated", args);
#endif
			
			GameManager.Instance.DisplayLimitedBoosterManager(ApplicationManager.config.game.delayBetweenLimitedBooster + ApplicationManager.config.game.boosterTimer[(int)m_clickedBoosterType]);
			
			switch (m_clickedBoosterType)
			{				
				default:
					break;
			}
		}
		

		public void ShowEndLevelPopup (BigInteger collectedGold)
		{
			ApplicationManager.datas.level++;
			//UIManager.OpenPopup<EndLevelPopup>();
		}

		private void OnClickTab ( TabButton.Tab tab )
		{
			for (int i = 0; i < m_tabs.Length; i++)
			{
				if (i == 1)
				{
                    m_tabButtons[i].SetActive(i == (int)tab);
                }
                else if(i !=1)
				{
					m_tabButtons[i].SetActive(i == (int)tab);
				}
				m_tabs[i].SetActive(i == (int)tab);
			}
        }

		private void UpdateCoinsDisplay ()
		{
			moneyText.text = MathHelper.ConvertToEngineeringNotation(ApplicationManager.datas.coins, 4) + " $";
			m_bounceSequence = DOTween.Sequence();

			m_bounceSequence.Append(moneyText.transform.DOScale(UVector3.one * 0.85f, 0.1f).SetEase(Ease.OutCubic));
			m_bounceSequence.Append(moneyText.transform.DOScale(UVector3.one * 1.1f, 0.1f).SetEase(Ease.OutCubic));
			m_bounceSequence.Append(moneyText.transform.DOScale(UVector3.one, 0.1f).SetEase(Ease.OutBounce));
		}
		

		private void OnUTCTimeUpdated ()
		{
			CheckOfflineEarning();
		}

		private void CheckOfflineEarning ()
		{
			//if (!ApplicationManager.datas.haveCompleteTutorial)
			//	return;
			if (ApplicationManager.config.application.enableOfflineEarning == false)
				return;

			m_needOfflineEarningCheck = false;
#if UNITY_EDITOR
			if (ApplicationManager.timeSinceLastAppQuit.TotalSeconds >= 30f)
#endif
#if !UNITY_EDITOR
			if (ApplicationManager.timeSinceLastAppQuit.TotalMinutes >= 30f)
#endif
			{
				GameConfig.GameSettings gamesettings = ApplicationManager.config.game;
				
				BigInteger offlineEarning = (Mathf.Min((int)ApplicationManager.timeSinceLastAppQuit.TotalSeconds, 48 * 3600)) * Upgrade.GetOfflineEarning()/ 3600;

				if (offlineEarning > 0)
				{
					OfflineEarningPopup popup = UIManager.OpenPopup<OfflineEarningPopup>();
					popup.SetValue(offlineEarning);
				}
				
			}
		}

		private void OnBoosterVideoEnd ( RewardedVideoEvent adEvent )
		{
			switch (adEvent)
			{
				case RewardedVideoEvent.Closed:
					this.UIManager.CloseProcessingPopup();
					m_rewardedVideoStarted = false;
					break;

				case RewardedVideoEvent.Failed:
					this.UIManager.CloseProcessingPopup();
					m_rewardedVideoStarted = false;
					GameManager.Instance.DisplayLimitedBoosterManager(ApplicationManager.config.game.delayBetweenLimitedBooster);
					break;

				case RewardedVideoEvent.Rewarded:

					UIManager.sceneManager.rewardedShown = true;
					ActivateBooster();
					this.UIManager.CloseProcessingPopup();
					m_rewardedVideoStarted = false;
					break;

				default:
					break;
			}
		}
		
		private void OnLimitedBoosterDisplayExpire ()
		{
			GameManager.Instance.DisplayLimitedBoosterManager(ApplicationManager.config.game.delayBetweenLimitedBooster);
		}

		public void DisplayLimitedBooster()
		{
			int maxValue = Enum.GetValues(typeof(Boosters.BoosterType)).Length;
			int boosterValue = 0;

			do
			{
				boosterValue = URandom.Range(0, maxValue);
			} while (boosterValue == m_lastLimitedBoosterIndex);

			m_lastLimitedBoosterIndex = boosterValue;
			if (OnDisplayLimitedBooster != null)
			{
				OnDisplayLimitedBooster.Invoke((Boosters.BoosterType)boosterValue);
			}
		}

		private void OnStartButtonClick()
		{
			UIManager.OpenPanel<GamePanel>();
			UIManager.sceneManager.gameManager.StartGame();
		}

	}

}