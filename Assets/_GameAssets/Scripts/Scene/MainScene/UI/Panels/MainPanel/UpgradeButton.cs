﻿using Pinpin.Helpers;
using Pinpin.Scene.MainScene.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using UnityEngine.UI;

namespace Pinpin.UI
{

    public class UpgradeButton : MonoBehaviour
    {

        public static Action<Upgrade.UpgradeType> onClick;
        [SerializeField] private PushButton upgradeButton;
        [SerializeField] private string m_nameOfBoostedStat;
        [SerializeField] private Text m_levelText;
        [SerializeField] private Text m_upgradeText;
        [SerializeField] private GameObject m_maxLevelIndicator;
        [SerializeField] private Image m_gauge;
        [SerializeField] private Upgrade.UpgradeType upgradeType;
        [SerializeField] private Image upgradeArrow;
        [SerializeField] private ColorBlock m_stepColors;
        [SerializeField] private bool m_hasSteps;
        [SerializeField] private int m_stepSize = 10;
        [SerializeField] private int m_max = 10;

        private ColorBlock m_classicColors;
        private Color m_arrowClassicColor;
        private BigInteger m_cost;
        private int m_level;
        private int m_maxLevel;

        void Start()
        {
            m_classicColors = upgradeButton.colors;
            m_arrowClassicColor = upgradeArrow.color;
            upgradeButton.onClick += OnUpgradeButtonClick;
            ApplicationManager.onCoinsChange += UpdateState;
            UpdateValues();
            m_maxLevel = ApplicationManager.datas.GetUpgradeLevel((int)upgradeType);
        }

        private void OnDestroy()
        {
            upgradeButton.onClick -= OnUpgradeButtonClick;
            ApplicationManager.onCoinsChange -= UpdateState;
        }

        private void OnUpgradeButtonClick()
        {
            if (onClick != null)
                onClick.Invoke(upgradeType);

            UpdateValues();
        }

        private void UpdateValues()
        {
            m_level = ApplicationManager.datas.GetUpgradeLevel((int)upgradeType);
            m_cost = ApplicationManager.config.GetUpgradeCost(upgradeType, m_level + 1);
            m_levelText.text = "LV." + m_level.ToString(ApplicationManager.currentCulture.NumberFormat);
            m_gauge.fillAmount = (float)m_level / (float)m_max;
			
            m_upgradeText.text = "+" + (m_level + 1) * 10 + " " + m_nameOfBoostedStat;
            upgradeButton.text = "$" + MathHelper.ConvertToEngineeringNotation(m_cost, 3);
            UpdateState();
        }

        private void UpdateState()
        {
            if (m_level == 0)
            {
                m_levelText.text = "New";
            }
            if (m_hasSteps)
            {
                if ((m_level + 1) % m_stepSize == 0)
                {
                    upgradeButton.colors = m_stepColors;
                    upgradeArrow.color = m_stepColors.normalColor;
                }
                else
                {
                    upgradeButton.colors = m_classicColors;
                    upgradeArrow.color = m_arrowClassicColor;
                }
				
            }


            bool isUpgradable = m_cost <= ApplicationManager.datas.coins;
            if (m_level >= m_max)
            {
                isUpgradable = false;
                upgradeButton.text = "";
                m_maxLevelIndicator.SetActive(true);
                m_upgradeText.text = "";
            }
            else
            {
                m_maxLevelIndicator.SetActive(false);
            }
			

            upgradeButton.isInteractable = isUpgradable;
            upgradeArrow.enabled = isUpgradable;
        }

    }

}