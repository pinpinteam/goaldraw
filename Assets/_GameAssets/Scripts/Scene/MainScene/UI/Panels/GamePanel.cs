﻿using Pinpin.UI;
using UnityEngine;
using UnityEngine.UI;
using Pinpin.Helpers;
using DG.Tweening;

namespace Pinpin.Scene.MainScene.UI
{

	public sealed class GamePanel : AUIPanel
	{

		private new MainSceneUIManager UIManager
		{
			get { return (base.UIManager as MainSceneUIManager); }
		}

		[Header("Top UI")]
		[SerializeField] private ParticleSystem m_moneyPS;
		[SerializeField] private Text moneyText;
		[SerializeField] private Text m_currentLevelText;
		[SerializeField] private Text m_nextLevelText;
		[SerializeField] private Image m_progressionGauge;

		private void OnEnable ()
		{
			UpdateLevel();
			UpdateCoinsDisplay();
		}

		public void UpdateLevel ()
		{
			m_currentLevelText.text = ApplicationManager.datas.level.ToString();
			m_nextLevelText.text = (ApplicationManager.datas.level + 1).ToString();
		}

		private void UpdateCoinsDisplay ()
		{
			moneyText.text = MathHelper.ConvertToEngineeringNotation(ApplicationManager.datas.coins, 4) + " $";
			Sequence bounceSequence = DOTween.Sequence();

			bounceSequence.Append(moneyText.transform.DOScale(Vector3.one * 0.85f, 0.1f).SetEase(Ease.OutCubic));
			bounceSequence.Append(moneyText.transform.DOScale(Vector3.one * 1.1f, 0.1f).SetEase(Ease.OutCubic));
			bounceSequence.Append(moneyText.transform.DOScale(Vector3.one, 0.1f).SetEase(Ease.OutBounce));
		}
	}
}