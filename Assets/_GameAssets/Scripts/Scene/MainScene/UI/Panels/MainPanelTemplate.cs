﻿using AdsManagement;
using Pinpin.UI;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Pinpin.Scene.MainScene.UI
{

	public sealed class MainPanelTemplate: AUIPanel
	{
		private bool m_rewardedVideoRewarded = false;

		private new MainSceneUIManager UIManager
		{
			get { return (base.UIManager as MainSceneUIManager); }
		}

		protected override void Awake()
		{
			base.Awake();
		}

		protected override void OnDestroy()
		{
			base.OnDestroy();
		}

		private void Start ()
		{
			ApplicationManager.onRewardedVideoAvailabilityChange += EnableVideoButton;
		}

		private void EnableVideoButton ( bool enabled )
		{
		}
		
		private void Update ()
		{
		}
	}

}