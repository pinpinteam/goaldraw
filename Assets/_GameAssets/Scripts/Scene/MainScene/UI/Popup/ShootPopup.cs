﻿using UnityEngine;
using Pinpin.UI;

namespace Pinpin.Scene.MainScene.UI
{

	public sealed class ShootPopup: AClosablePopup
	{

		private new MainSceneUIManager UIManager
		{
			get { return (base.UIManager as MainSceneUIManager); }
		}

		protected override void OnClose()
		{
			GameManager.Instance.StartShoot();
		}

	}

}