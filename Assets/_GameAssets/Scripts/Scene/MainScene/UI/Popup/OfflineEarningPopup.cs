﻿using DG.Tweening;
using MoreMountains.NiceVibrations;
using Pinpin.Helpers;
using Pinpin.UI;
using System;
using System.Collections;
using System.Numerics;
using UnityEngine;
using UnityEngine.UI;

namespace Pinpin.Scene.MainScene.UI
{
	public class OfflineEarningPopup : AClosablePopup
	{
		[SerializeField] private Text m_coinGainText;
		[SerializeField] private PushButton m_collectCoinsButton;
		[SerializeField] private PushButton m_collectCoinsRewardButton;


		private BigInteger m_coinGain;
		
		private bool m_rewardedVideoRewarded;
		private bool m_hasCollectCoins;

		private new MainSceneUIManager UIManager
		{
			get { return (base.UIManager as MainSceneUIManager); }
		}

		protected void Start ()
		{
			m_collectCoinsButton.onClick += OnCollectCoins;
			m_collectCoinsRewardButton.onClick += OnCollectRewardedClick;
		}

		protected override void OnDestroy ()
		{
			m_collectCoinsButton.onClick -= OnCollectCoins;
			m_collectCoinsRewardButton.onClick -= OnCollectRewardedClick;
			base.OnDestroy();
		}

		public void SetValue(BigInteger value)
		{
			m_hasCollectCoins = false;
			m_coinGain = value;
			m_coinGainText.text = "$ " + MathHelper.ConvertToEngineeringNotation(m_coinGain, 3);

			m_collectCoinsButton.isInteractable = true;
			m_collectCoinsRewardButton.isInteractable = true;


		}
		
		private void DisableCollectButtons ()
		{
			m_collectCoinsButton.isInteractable = false;
			m_collectCoinsRewardButton.isInteractable = false;
		}

		public override bool IsClosable ()
		{
			return false;
		}

		private void OnCollectCoins ()
		{
			if (!m_hasCollectCoins)
			{
				FXManager.Instance.PlayMoney();
				m_hasCollectCoins = true;
				DisableCollectButtons();
				MMVibrationManager.Haptic(HapticTypes.MediumImpact);

				ApplicationManager.AddCoins(m_coinGain);
				UIManager.CloseActivePopup(true);
			}
		}

#region Collect x3

		private void OnCollectRewardedClick ()
        {
#if MADPINPIN_ADS
			UIManager.sceneManager.ShowRewardedVideo(OnCollectVideoEnd, "double_offline_earnings");
			{
				DisableCollectButtons();
				m_rewardedVideoRewarded = false;
			}
#endif
		}

		private void OnCollectVideoEnd ( AdsManagement.RewardedVideoEvent adEvent )
		{
			switch (adEvent)
			{
				case AdsManagement.RewardedVideoEvent.Closed:
					if (!m_rewardedVideoRewarded)
					{
						m_collectCoinsButton.isInteractable = true;
						m_collectCoinsRewardButton.isInteractable = true;
						this.UIManager.CloseProcessingPopup();
					}
					break;

				case AdsManagement.RewardedVideoEvent.Failed:
					m_collectCoinsButton.isInteractable = true;
					m_collectCoinsRewardButton.isInteractable = true;
					this.UIManager.CloseProcessingPopup();
					break;

				case AdsManagement.RewardedVideoEvent.Rewarded:
					FXManager.Instance.PlayMoney(true);
					m_rewardedVideoRewarded = true;
					UIManager.sceneManager.rewardedShown = true;
					ApplicationManager.AddCoins(m_coinGain * 2);
					UIManager.CloseActivePopup(true);
					this.UIManager.CloseProcessingPopup();
					break;

				default:
					break;
			}
		}

#endregion
	}
}
