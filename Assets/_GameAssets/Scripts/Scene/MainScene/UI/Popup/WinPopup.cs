﻿using UnityEngine;
using Pinpin.UI;
using MoreMountains.NiceVibrations;

namespace Pinpin.Scene.MainScene.UI
{

	public sealed class WinPopup: AClosablePopup
	{

		private new MainSceneUIManager UIManager
		{
			get { return (base.UIManager as MainSceneUIManager); }
		}

		protected override void OnClose()
		{
			base.OnClose();
			GameManager.Instance.StartGame();
			MMVibrationManager.Haptic(HapticTypes.MediumImpact);
		}

	}

}