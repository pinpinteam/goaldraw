﻿using UnityEngine;
using Pinpin.UI;
using MoreMountains.NiceVibrations;

namespace Pinpin.Scene.MainScene.UI
{

	public sealed class LosePopup: AClosablePopup
	{

		private new MainSceneUIManager UIManager
		{
			get { return (base.UIManager as MainSceneUIManager); }
		}

		protected override void OnClose()
		{
			base.OnClose();
			GameManager.Instance.ResetGame();
			MMVibrationManager.Haptic(HapticTypes.MediumImpact);
		}

	}

}