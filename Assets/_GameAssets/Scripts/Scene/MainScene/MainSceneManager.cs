﻿using System;
using System.Collections;
using Pinpin.Scene.MainScene.UI;
using Pinpin.UI;
using UnityEngine;
using DG.Tweening;
using System.Collections.Generic;
using PaperPlaneTools;
#if MADPINPIN
using MadboxAds = MadBox.Services.ExternalServices.Ads;
#endif

namespace Pinpin.Scene.MainScene
{

	public sealed class MainSceneManager : ASceneManager
	{

		private new MainSceneUIManager UI
		{
			get { return (base.UI as MainSceneUIManager); }
		}

		public GameManager gameManager;



#region MonoBehaviour

		protected override void Awake ()
		{
			base.Awake();
			if (!ApplicationManager.isInitialized)
				return;
			
		}

		protected override void Start ()
		{
			base.Start();
			this.UI.OpenPanel<MainPanel>();

			if (UI.GetPopup<RatingPopup>().GetComponent<IAlertPlatformAdapter>() != null)
			{
				RateBox.Instance.AlertAdapter = UI.GetPopup<RatingPopup>().GetComponent<IAlertPlatformAdapter>();
			}

			base.SetSceneReady();
			SetEventListener();
		}

		public void AddCoins ( int m_coinGain )
		{
			throw new NotImplementedException();
		}

		public void SetEventListener()
		{
			gameManager.OnGameWin += GameManager_OnGameWin;
			gameManager.OnGameLoose += GameManager_OnGameLoose;
			gameManager.OnGameStart += GameManager_OnGameStart;
		}

		public void UnsetEventListener()
		{
			gameManager.OnGameWin -= GameManager_OnGameWin;
			gameManager.OnGameLoose -= GameManager_OnGameLoose;
			gameManager.OnGameStart -= GameManager_OnGameStart;
		}

		private void GameManager_OnGameStart()
		{
			UI.GetPanel<GamePanel>().UpdateLevel();
		}

		private void GameManager_OnGameWin()
		{
			UI.OpenPopup<WinPopup>();
		}

		private void GameManager_OnGameLoose()
		{
			UI.OpenPopup<LosePopup>();
		}

		#endregion

		#region Ads

		public void ShowInterstitial ()
		{
#if MADPINPIN_ADS
			if (ApplicationManager.canWatchInterstitial)
			{
				this.UI.OpenProcessingPopup("Loading ad");
				RSG.IPromise<bool> promise = MadboxAds.AdsCenter.Instance.DisplayEndGameInterstitial();
				promise.Then((bool success) => { UI.CloseProcessingPopup(); }).Catch((System.Exception e) => {
					UI.CloseProcessingPopup();
					Debug.LogError(e.Message);
				});
			}
#endif
		}
		

		public bool ShowRewardedVideo ( Action<AdsManagement.RewardedVideoEvent> adEvent, string location )
		{
#if MADPINPIN_ADS
			if (ApplicationManager.canWatchRewardedVideo)
			{
				this.UI.OpenProcessingPopup("Loading ad");
				RSG.IPromise<bool> promise = MadboxAds.AdsCenter.Instance.DisplayRewarded(location);
				promise.Then(( bool success ) => {
					adEvent(success?AdsManagement.RewardedVideoEvent.Rewarded: AdsManagement.RewardedVideoEvent.Failed); }).Catch(( System.Exception e ) => {
						adEvent(AdsManagement.RewardedVideoEvent.Failed);
						Debug.LogError(e.Message);
				});
				return true;
			}
#endif
			return false;
		}

		public void CloseActivePopup(bool force = false)
		{
			UI.CloseActivePopup(force);
		}

#endregion
#region ASceneManager

		public override void OnQuit ()
		{
			ApplicationManager.Quit();
		}

		public bool rewardedShown { get; internal set; }


		#endregion

		private void OnDestroy()
		{
			UnsetEventListener();
		}

	}

}