﻿using UnityEngine.Events;

namespace AdsManagement
{

	public enum InterstitialEvent
	{
		Opened,
		Closed,
		Failed
	}

	public enum RewardedVideoEvent
	{
		Opened,
		Closed,
		Rewarded,
		Failed
	}

	public enum BannerEvent
	{
		Loaded,
		LoadFailed,
		Clicked,
		Dismissed
	}

}