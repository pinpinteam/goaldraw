﻿using System;
using System.Collections;
using System.Collections.Generic;
#if MADPINPIN
using Facebook.Unity.Settings;
#endif
using Pinpin;
using UnityEditor;
using UnityEditor.Build.Reporting;
using UnityEngine;

public class PinpinStatusWindow : EditorWindow
{
	[MenuItem("Pinpin/Show status Window", false, 0)]
	static void ShowStatusWindow ()
	{
		GetWindow(typeof(PinpinStatusWindow));
	}

	private Pinpin.BuildTargetList m_buildTargetList;
#if MADPINPIN
	private GameAnalyticsSDK.Setup.Settings m_gameAnalyticsSettings;
#endif
	private GameConfig m_config;
	private Vector2 scrollPos;

	private void Awake ()
	{
		Loadbuildist();
	}

	private void OnFocus ()
	{
		Loadbuildist();
	}

	private void Loadbuildist ()
	{
		if (m_buildTargetList == null)
		{
			m_buildTargetList = AssetDatabase.LoadAssetAtPath<Pinpin.BuildTargetList>("Assets/Objects/BuildTargetList.asset");
		}

#if MADPINPIN
		m_gameAnalyticsSettings = AssetDatabase.LoadAssetAtPath<GameAnalyticsSDK.Setup.Settings>("Assets/Resources/GameAnalytics/Settings.asset");
		m_config = AssetDatabase.LoadAssetAtPath<GameConfig>("Assets/_GameAssets/Objects/GameConfig.asset");
#endif
	}

	private void OnGUI ()
	{
		GUIStyle errorStyle = new GUIStyle(EditorStyles.boldLabel);
		errorStyle.normal.textColor = new Color (0.8627452f, 0.2f, 0.1882353f);

		GUIStyle goodStyle = new GUIStyle(EditorStyles.boldLabel);
		goodStyle.normal.textColor = new Color(48f / 255f, 209f / 255f, 88f / 255f);


		EditorGUILayout.BeginVertical(EditorStyles.helpBox);

		EditorGUILayout.BeginHorizontal();
		GUILayout.Label("MadPinpin Package status :");
		bool madpinpinLoaded = AssetDatabase.IsValidFolder("Assets/MadPinpin");
		if (!madpinpinLoaded)
		{
			GUILayout.Label("Not Loaded", errorStyle, GUILayout.ExpandWidth(false));
			if (GUILayout.Button("Import It", GUILayout.ExpandWidth(false)))
			{
				AssetDatabase.ImportPackage(Application.dataPath + "/Packages/MadBoxAnalyticsPackage.unitypackage", false);
				PinpinMenu.AddDefineSymbol("MADPINPIN");
			}
		}
		else
		{
			GUILayout.Label("Imported", goodStyle, GUILayout.ExpandWidth(false));
		}
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();
		GUILayout.Label("MADPINPIN Define symbol :");

		string androidSymbolsString = PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android);
		string iosSymbolsString = PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.iOS);
		if (!androidSymbolsString.Contains(";MADPINPIN") || !iosSymbolsString.Contains(";MADPINPIN"))
		{
			GUILayout.Label("Missing", errorStyle, GUILayout.ExpandWidth(false));
			if (madpinpinLoaded && GUILayout.Button("Add It", GUILayout.ExpandWidth(false)))
			{
				PinpinMenu.AddDefineSymbol("MADPINPIN");
			}
		}
		else
		{
			GUILayout.Label("Defined", goodStyle, GUILayout.ExpandWidth(false));
		}
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();
		GUILayout.Label("Facebook App ID :");

#if MADPINPIN
		if (!FacebookSettings.IsValidAppId)
		{
			GUILayout.Label("Invalid", errorStyle, GUILayout.ExpandWidth(false));
			if (madpinpinLoaded && GUILayout.Button("Select Settings", GUILayout.ExpandWidth(false)))
			{
				Selection.activeObject = AssetDatabase.LoadAssetAtPath< FacebookSettings>("Assets/FacebookSDK/SDK/Resources/FacebookSettings.asset");
			}
		}
		else
		{
			GUILayout.Label(FacebookSettings.AppId, goodStyle, GUILayout.ExpandWidth(false));
		}
#else
		GUILayout.Label("Add MADPINPIN define first", errorStyle, GUILayout.ExpandWidth(false));
#endif
		EditorGUILayout.EndHorizontal();


		EditorGUILayout.BeginHorizontal();
		GUILayout.Label("Game Analytics Settings :");

#if MADPINPIN
		int index = -1;

		for (int i = 0; i < m_gameAnalyticsSettings.Platforms.Count; i++)
		{
			if (EditorUserBuildSettings.activeBuildTarget == UnityEditor.BuildTarget.iOS && m_gameAnalyticsSettings.Platforms[i] == RuntimePlatform.IPhonePlayer)
			{
				index = i;
			}
			else if(EditorUserBuildSettings.activeBuildTarget == UnityEditor.BuildTarget.Android && m_gameAnalyticsSettings.Platforms[i] == RuntimePlatform.Android)
			{
				index = i;
			}
		}

		if (index == -1 || string.IsNullOrEmpty(m_gameAnalyticsSettings.GetGameKey(index)) || string.IsNullOrEmpty(m_gameAnalyticsSettings.GetSecretKey(index)))
		{
			GUILayout.Label("Invalid", errorStyle, GUILayout.ExpandWidth(false));
			if (madpinpinLoaded && GUILayout.Button("Select Settings", GUILayout.ExpandWidth(false)))
			{
				Selection.activeObject = m_gameAnalyticsSettings;
			}
		}
		else
		{
			GUILayout.Label("Done", goodStyle, GUILayout.ExpandWidth(false));
		}
#else
		GUILayout.Label("Add MADPINPIN define first", errorStyle, GUILayout.ExpandWidth(false));
#endif
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();
		GUILayout.Label("Aplitude Settings :");

#if MADPINPIN

		if (string.IsNullOrEmpty(m_config.application.amplitudeAPIKey))
		{
			GUILayout.Label("Invalid", errorStyle, GUILayout.ExpandWidth(false));
			if (madpinpinLoaded && GUILayout.Button("Select Config", GUILayout.ExpandWidth(false)))
			{
				Selection.activeObject = m_config;
			}
		}
		else
		{
			GUILayout.Label(m_config.application.amplitudeAPIKey, goodStyle, GUILayout.ExpandWidth(false));
		}
#else
		GUILayout.Label("Add MADPINPIN define first", errorStyle, GUILayout.ExpandWidth(false));
#endif
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.EndVertical();


		if (m_buildTargetList != null)
		{
			errorStyle.fontSize = 20;
			EditorGUILayout.BeginVertical(EditorStyles.helpBox);
			GUILayout.Label("Current Build Target : " + PlayerPrefs.GetString("CurrentTarget", "None"), errorStyle);
			EditorGUILayout.BeginHorizontal();
			
			EditorGUI.BeginChangeCheck();

			m_buildTargetList.buildPath = EditorGUILayout.TextField(m_buildTargetList.buildPath);
			if (GUILayout.Button("Select folder", GUILayout.ExpandWidth(false)))
			{
				string newFolder = EditorUtility.OpenFolderPanel("Select build folder", m_buildTargetList.buildPath, "");
				if (newFolder.Length > 0)
					m_buildTargetList.buildPath = newFolder;
			}
			EditorGUILayout.EndHorizontal();
			

			EditorGUILayout.EndVertical();

			scrollPos = GUILayout.BeginScrollView(scrollPos, false, false, GUILayout.ExpandHeight(true));

			GUILayout.BeginVertical();
			for (int i = 0; i < m_buildTargetList.buildTargets.Count; i++)
			{
				if (DrawBuildTarget(m_buildTargetList.buildTargets[i]))
				{
					m_buildTargetList.buildTargets.RemoveAt(i);
					i--;
				}
			}
			GUILayout.EndVertical();
			if (GUILayout.Button("Add build target"))
			{
				m_buildTargetList.buildTargets.Add(new BuildTargetList.BuildTarget());
			}
			if (EditorGUI.EndChangeCheck())
			{
				EditorUtility.SetDirty(m_buildTargetList);
			}
			GUILayout.EndScrollView();
		}
		else
		{
			GUILayout.Label("Error : Missing BuildTargetList.asset", errorStyle);
			if (GUILayout.Button("Create It", GUILayout.ExpandWidth(true)))
			{
				m_buildTargetList = ScriptableObject.CreateInstance<Pinpin.BuildTargetList>();

				AssetDatabase.CreateAsset(m_buildTargetList, "Assets/Objects/BuildTargetList.asset");
				AssetDatabase.SaveAssets();

			}
		}
	}

	private bool DrawBuildTarget ( BuildTargetList.BuildTarget buildTarget )
	{
		bool destroy = false;
		EditorGUILayout.BeginVertical(EditorStyles.helpBox);
		{
			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.Label("Build target name : ");
				if (GUILayout.Button("Delete build target", GUILayout.ExpandWidth(false)))
				{
					destroy = true;
				}
			}
			EditorGUILayout.EndHorizontal();

			buildTarget.buildName = GUILayout.TextField(buildTarget.buildName);
			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.Label("Defines : ", GUILayout.ExpandWidth(false));
				EditorGUILayout.BeginVertical();
				{
					for (int i = 0; i < buildTarget.defines.Count; i++)
					{
						EditorGUILayout.BeginHorizontal();
						{
							buildTarget.defines[i] = EditorGUILayout.TextField(buildTarget.defines[i], GUILayout.ExpandWidth(true));
							if (GUILayout.Button("-", GUILayout.ExpandWidth(false)))
							{
								buildTarget.defines.RemoveAt(i);
								i--;
							}
						}
						EditorGUILayout.EndHorizontal();
					}
				}
				EditorGUILayout.EndVertical();
			}
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.BeginHorizontal();
			{
				if (GUILayout.Button("Add define", GUILayout.ExpandWidth(true)))
				{
					buildTarget.defines.Add("");
				}
				if (GUILayout.Button("Select Target", GUILayout.ExpandWidth(true)))
				{
					SetupTarget(buildTarget);
				}
				if (GUILayout.Button("Build", GUILayout.ExpandWidth(true)))
				{
					BuildTarget(buildTarget);
				}
			}
			EditorGUILayout.EndHorizontal();
		}
		GUILayout.EndVertical();

		return destroy;
	}

	private void SetupTarget ( Pinpin.BuildTargetList.BuildTarget buildTarget )
	{
		m_buildTargetList.OnValidate();
		foreach (string define in m_buildTargetList.allDefines)
		{
			PinpinMenu.RemoveDefineSymbol(define);
		}

		for (int i = 0; i < buildTarget.defines.Count; i++)
		{
			PinpinMenu.AddDefineSymbol(buildTarget.defines[i]);
		}
		PlayerPrefs.SetString("CurrentTarget", buildTarget.buildName);
	}

	private void BuildTarget ( Pinpin.BuildTargetList.BuildTarget buildTarget )
	{
		if (EditorUtility.DisplayDialog("Build", "Build current target : " + PlayerPrefs.GetString("CurrentTarget", "None"), "yes", "no"))
		{
			SetupTarget(buildTarget);
			List<string> scenes = new List<string>();
			for (int i = 0; i < EditorBuildSettings.scenes.Length; i++)
			{
				if (EditorBuildSettings.scenes[i].enabled)
				{
					scenes.Add(EditorBuildSettings.scenes[i].path);
				}
			}
			BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
			buildPlayerOptions.scenes = scenes.ToArray();
			buildPlayerOptions.locationPathName = m_buildTargetList.buildPath + "/"
												+ Application.productName + "-"
												+ buildTarget.buildName + "-"
												+ Application.version + "-"
												+ (EditorUserBuildSettings.activeBuildTarget == UnityEditor.BuildTarget.iOS ? PlayerSettings.iOS.buildNumber : PlayerSettings.Android.bundleVersionCode.ToString())
												+ (EditorUserBuildSettings.activeBuildTarget == UnityEditor.BuildTarget.Android ? ".apk" : "");

			buildPlayerOptions.target = EditorUserBuildSettings.activeBuildTarget;
			buildPlayerOptions.options = BuildOptions.None;

			BuildReport report = BuildPipeline.BuildPlayer(buildPlayerOptions);
			BuildSummary summary = report.summary;

			if (summary.result == BuildResult.Succeeded)
			{
				Debug.Log("Build succeeded: " + summary.totalSize + " bytes");
			}

			if (summary.result == BuildResult.Failed)
			{
				Debug.Log("Build failed");
			}
		}

	}

}
