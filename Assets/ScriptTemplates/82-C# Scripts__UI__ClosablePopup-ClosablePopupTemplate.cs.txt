﻿using UnityEngine;
using Pinpin.UI;

namespace Pinpin.Scene.MainScene.UI
{

	public sealed class #SCRIPTNAME#: AClosablePopup
	{

		private new MainSceneUIManager UIManager
		{
			get { return (base.UIManager as MainSceneUIManager); }
		}

	}

}